import requests
import csv
from bs4 import BeautifulSoup

# link
page = requests.get("https://www.newegg.com/p/pl?N=4814%2050001306&d=western%20digital%20easystore&PageSize=96").text
soup = BeautifulSoup(page, "html.parser")

# tags
title = soup.findAll("a",{"class": "item-title"})
price = soup.findAll("li",{"class": "price-current"})
shipping = soup.findAll("li",{"class": "price-ship"})

# name the output file
out_filename = "Western_Digital_Drives.csv"

# csv header
headers = "title,price,shipping,"

# opens file, and writes headers
f = open(out_filename, "w")
f.write(headers)
f.write('\n')

# bad chars to remove

bad_chars1 = '\xa0'
bad_chars2 = '\r'
bad_chars3 = '\n'
bad_chars4 = '–'
bad_chars5 = ','



# Outputs All Products

with open('Western_Digital_Drives.csv', 'w', encoding='utf_8_sig') as myfile:
    wr = csv.writer(myfile)
    
    # outputs first item

    f.write(title[0].text.replace(bad_chars1,'').replace(bad_chars2,'').replace(bad_chars3,'').replace(bad_chars4,'').replace(bad_chars5,'').strip())
    f.write(',')
    f.write(price[0].text.replace(bad_chars1,' ').replace(bad_chars2,'').replace(bad_chars3,'').replace(bad_chars4,'').replace(bad_chars5,'').replace(bad_chars6,'').strip())
    f.write(',')
    f.write(shipping[0].text.replace(bad_chars1,'').replace(bad_chars2,'').replace(bad_chars3,'').replace(bad_chars4,'').replace(bad_chars5,'').strip())
    f.write('\n')

    # outputs rest of items
    for t,p,s in zip(title,price,shipping):
         myfile.write(
            f"{t.text.replace(bad_chars1,'').replace(bad_chars2,'').replace(bad_chars3,'').replace(bad_chars4,'').replace(bad_chars5,'').strip()},"
            f"{p.text.replace(bad_chars1,' ').replace(bad_chars2,'').replace(bad_chars3,'').replace(bad_chars4,'').replace(bad_chars5,'').strip()},"
            f"{s.text.replace(bad_chars1,'').replace(bad_chars2,'').replace(bad_chars3,'').replace(bad_chars4,'').replace(bad_chars5,'').strip()}"
            '\n'
            )

# closes csv file

myfile.close()